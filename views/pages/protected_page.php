<?php
	$UserManager = new UserManager();
	if($UserManager->login_check() == true): ?>
	<h1>Welcome <?php echo htmlentities($_SESSION['username']); ?></h1>
	<p>Return to <a href="index.php">Home</a></p>
<?php else: ?>
	<p>
		You are not authorized to access this page. <a href="index.php">Home</a>
	</p>
<?php endif; ?>