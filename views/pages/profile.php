<div name="body" class="container">
	<div name="head" class="row" style="height: 225px;">
		<div name="cover_photo" class="cover_div img-responsive">
			<div name="photo_and_name" class="col-md-5 col-md-offset-1" style="top: 75px;">
				<div name="profile_image" class="img_div">
					<img src="../../img/profile_img.jpg" alt="Responsive imge" class="img-responsive img-circle">
				</div>
				<div name ="profile_name" class="nick_name_positon">
					<h3>OD Master</h3>
				</div>
			</div>
		</div>
		<div name="button_location" class="button_position">
			<button class="btn btn-success">Edit Profile</button>
		</div>
	</div>
	<div name="profile_text" class="row">
		<div name = "status_location" class="profile_status_location">
			<div name = "status_header" class="header_text">
				<h3>Profile</h3>
			</div>
			<div name="status" class="status_text">
				<p>Name: Tanakorn Malajak</p>
				<p>Faculty: Engineering</p>
				<p>Major: Information System and Network Engineering</p>
				<p>Student Code: 580611024</p>
			</div>
		</div>
	</div>
</div>


<style>

/*cover image size*/
	.cover_div{
		width: 100%;
		height: 150px;
   		background-repeat: no-repeat;
		background-color: #9e9e9e; /*change to image*/
	}
/*profile image size*/
	.img_div{
		width: 150px;
		height: 150px;
		float: left;
	}
	.nick_name_positon{
		position: relative;
		top: 80.5px;
		left: 10px;
	}
	.button_position{
		position: relative;
		top: 20.5px;
		text-align: right;
		margin-right: 20px;
	}
	.profile_status_location{
		position: relative;
		margin: auto;
		margin-left: 200px;
		margin-right: 200px;
		margin-top: 50px;
		margin-bottom: 50px;
		border-radius: 15px;
		background-color: #e5e5e5; /*change color*/
	}
	.header_text{
		text-align: center;
		padding-top: 3px;
		padding-bottom: 3px;
		background-color: #9e9e9e; /*change color*/
		border-top-left-radius: 15px;
		border-top-right-radius: 15px;
	}
	.status_text{
    	margin: auto;
    	margin-left: 25px;
    	margin-right: 25px;
    	padding-bottom: 20px;
    	padding-top: 20px;
    	font-size: 18px;
	}

</style>