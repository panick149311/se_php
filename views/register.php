<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript" src="js/sha512.js"></script>
	<script type="text/javascript" src="js/form.js"></script>
</head>
<body>
	<div class="container">
	<form method="post" action="index.php?controller=usermanager&action=register_na">
		<div class="form-group">
			<label for="email" class="col-sm-2 control-label">Email</label>
			<input type="email" class="form-control" id="email" name="email">
		</div>
		<div class="form-group">
			<label for="username" class="col-sm-2 control-label">Username</label>
			<input type="text" class="form-control" id="username" name="username">
		</div>
		<div class="form-group">
			<label for="password" class="col-sm-2 control-label">Password</label>
			<input type="password" class="form-control" id="password" name="password">
		</div>
		<div class="form-group">
			<label for="confirmpwd" class="col-sm-2 control-label">Confirm Password</label>
			<input type="password" class="form-control" id="confirmpwd" name="confirmpwd">
		</div>
		<div class="form-group">
			<input type="button" value="submit" class="btn btn-default" onclick="return regformhash(this.form, this.form.username, this.form.email, this.form.password, this.form.confirmpwd);">
		</div>
	</form>
	</div>
</body>
</html>